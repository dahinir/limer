var loopback = require('loopback');
var path = require('path');
var app = module.exports = loopback();
var started = new Date();

// Load up the push data source as the example cannot declaratively define
// the push data source in datasources.json as it cannot resolve the connector
// module by name
// var connector = require('loopback-push-notification');
// app.dataSources.push = loopback.createDataSource("push", {
//   "defaultForType": "push",
//   "connector": connector
// });

/*
 * 1. Configure LoopBack models and datasources
 *
 * Read more at http://apidocs.strongloop.com/loopback#appbootoptions
 */
//Don't combine using app.boot() and app.model(name, config) in multiple files because it may result in models being undefined due to race conditions. To avoid this when using app.boot() make sure all models are passed as part of the models definition.
//Initialize an application from an options object or a set of JSON and JavaScript files.
app.boot(__dirname);  // set the application root directory;

/*
 * 2. Configure request preprocessing
 *
 *  LoopBack support all express-compatible middleware.
 */

app.use(loopback.favicon());
app.use(loopback.logger(app.get('env') === 'development' ? 'dev' : 'default'));
app.use(loopback.cookieParser(app.get('cookieSecret')));
app.use(loopback.token({model: app.models.accessToken}));
app.use(loopback.bodyParser());
app.use(loopback.methodOverride());

/*
 * EXTENSION POINT
 * Add your custom request-preprocessing middleware here.
 * Example:
 *   app.use(loopback.limit('5.5mb'))
 */

/*
 * 3. Setup request handlers.
 */

// LoopBack REST interface
app.use(app.get('restApiRoot'), loopback.rest());
console.log( app.get('restApiRoot') );

// API explorer (if present)
try {
  var explorer = require('loopback-explorer')(app);
  app.use('/explorer', explorer);
  app.once('started', function(baseUrl) {
    console.log('Browse your REST API at %s%s', baseUrl, explorer.route);
  });
} catch(e){
  console.log(
    'Run `npm install loopback-explorer` to enable the LoopBack explorer'
  );
}

/*
 * EXTENSION POINT
 * Add your custom request-handling middleware here.
 * Example:
 *   app.use(function(req, resp, next) {
 *     if (req.url == '/status') {
 *       // send status response
 *     } else {
 *       next();
 *     }
 *   });
 */

// Let express routes handle requests that were not handled
// by any of the middleware registered above.
// This way LoopBack REST and API Explorer take precedence over
// express routes.
app.use(app.router);

// The static file server should come after all other routes
// Every request that goes through the static middleware hits
// the file system to check if a file exists.
app.use(loopback.static(path.join(__dirname, 'public')));

// Requests that get this far won't be handled
// by any middleware. Convert them into a 404 error
// that will be handled later down the chain.
app.use(loopback.urlNotFound());

/*
 * 4. Setup error handling strategy
 */

/*
 * EXTENSION POINT
 * Add your custom error reporting middleware here
 * Example:
 *   app.use(function(err, req, resp, next) {
 *     console.log(req.url, ' failed: ', err.stack);
 *     next(err);
 *   });
 */

// The ultimate error handler.
app.use(loopback.errorHandler());


/*
 * 5. Add a basic application status route at the root `/`.
 *
 * (remove this to handle `/` on your own)
 */

app.get('/', loopback.status());

/*
 * 6. Enable access control and token based authentication.
 */

var swaggerRemote = app.remotes().exports.swagger;
if (swaggerRemote) swaggerRemote.requireToken = false;

app.enableAuth();

/*
 * 7. Optionally start the server
 *
 * (only if this module is the main module)
 */

app.start = function() {
  return app.listen(function() {
    var baseUrl = 'http://' + app.get('host') + ':' + app.get('port');
    app.emit('started', baseUrl);
    console.log('LoopBack server listening @ %s%s', baseUrl, '/');
  });
};


/* noti start */
var Notification = app.models.notification;
var Application = app.models.application;
var PushModel = app.models.push;

function startPushServer() {
// Add our custom routes
  var badge = 1;
  app.post('/notify/:id', function (req, res, next) {
    var note = new Notification({
      // expirationInterval: 3600, // Expires 1 hour from now.
      badge: badge++,
      sound: 'ping.aiff',
      // alert: '\uD83D\uDCE7 \u2709 ' + 'Hello',
      alert: "t:"+ Date.now()
      // messageFrom: 'Ray'
    });

    PushModel.notifyById(req.params.id, note, function (err) {
      if (err) {
        console.error('Cannot notify %j: %s', req.params.id, err.stack);
        next(err);
        return;
      }
      console.log('.....pushing notification to %j', req.params.id);
      res.send(200, 'OK');
    });
  });

  PushModel.on('error', function (err) {
    console.error('Push Notification error: ', err.stack);
  });

// Pre-register an application that is ready to be used for testing.
// You should tweak config options in ./config.js

  var config = require('./config');
// console.log(config.apnsKeyData);
  var demoApp = {
    id: 'co.licky.app',
    userId: 'dummy@dumm.com',
    name: config.appName,

    description: 'LoopBack Push Notification Demo Application',
    pushSettings: {
      apns: {
        certData: config.apnsCertData,
        keyData: config.apnsKeyData,
        // key: 'cre/key.pem',
        // cert: 'cre/cert.pem',
        production: false,
        pushOptions: {
          // Extra options can go here for APN
        },
        feedbackOptions: {
          batchFeedback: true,
          interval: 300
        }
      },
      gcm: {
        serverApiKey: config.gcmServerApiKey
      }
    }
  };

  updateOrCreateApp(function (err, appModel) {
    if (err) throw err;
    console.log('Application id: %j', appModel.id);
  });

//--- Helper functions ---
  function updateOrCreateApp(cb) {
    Application.findOne({
        where: { name: demoApp.name }
      },
      function (err, result) {
        if (err) cb(err);
        if (result) {
          console.log('Updating application: ' + result.id);
          // console.log(result.pushSettings.apns.certData);
          // console.log('demoApp:'+ JSON.stringify(demoApp));
          result.updateAttributes(demoApp, cb);
          // console.log('demoApp:'+ JSON.stringify(result));
        } else {
          return registerApp(cb);
        }
      });
  }
  function registerApp(cb) {
    console.log('Registering a new Application...');
    // Hack to set the app id to a fixed value so that we don't have to change
    // the client settings
    Application.beforeSave = function (next) {
      if(this.name === demoApp.name) {
        this.id = 'co.licky.app';
      }
      next();
    };
    Application.register(
      demoApp.userId,  // 'put your developer id here',
      demoApp.name, //'put your unique application name here',
      {
        description: demoApp.description,
        pushSettings: demoApp.pushSettings
      },
      function (err, app) {
        if (err){
          console.log(" register app error");
          return cb(err);
        }
        console.log(" register app success");
        return cb(null, app);
      }
    );
  }
}
startPushServer();


/*
var apn = require('apn');
var config = require('./config');
var tokens = ["b924c35b16d3bfa719aef8ddae6846a6f0ab329da9b1cd90849bd33423b29ef9"];

if(tokens[0] == "<insert token here>") {
	console.log("Please set token to a valid device token for the push notification service");
	process.exit();
}

// Create a connection to the service using mostly default parameters.

// var service = new apn.connection({ gateway:'gateway.sandbox.push.apple.com' });
var service = new apn.connection(
  { gateway:'gateway.sandbox.push.apple.com',
// apns: {
  certData: config.apnsCertData,
  keyData: config.apnsKeyData
  // key: 'cre/key.pem',
  // cert: 'cre/cert.pem'
// }
});

service.on('connected', function() {
    console.log("Connected");
});

service.on('transmitted', function(notification, device) {
    console.log("Notification transmitted to:" + device.token.toString('hex'));
});

service.on('transmissionError', function(errCode, notification, device) {
    console.error("Notification caused error: " + errCode + " for device ", device, notification);
});

service.on('timeout', function () {
    console.log("Connection Timeout");
});

service.on('disconnected', function() {
    console.log("Disconnected from APNS");
});

service.on('socketError', console.error);


// If you plan on sending identical paylods to many devices you can do something like this.
function pushNotificationToMany() {
    var note = new apn.notification();
    note.setAlertText("Hello, from node-apn!");
    note.badge = 1;

    service.pushNotification(note, tokens);
}

pushNotificationToMany();


// If you have a list of devices for which you want to send a customised notification you can create one and send it to and individual device.
function pushSomeNotifications() {
    for (var i in tokens) {
        var note = new apn.notification();
        note.setAlertText("Hello, from node-apn! You are number: " + i);
        note.badge = i;

        service.pushNotification(note, tokens[i]);
    }
}

pushSomeNotifications();

/*
var apnConnection = new apn.Connection({
  production: false
});
var feedback = new apn.feedback({ address:'feedback.sandbox.push.apple.com', interval: 10 });

feedback.on('feedback', handleFeedback);
feedback.on('feedbackError', console.error);

function handleFeedback(feedbackData) {
	var time, device;
	for(var i in feedbackData) {
		time = feedbackData[i].time;
		device = feedbackData[i].device;

		console.log("Device: " + device.toString('hex') + " has been unreachable, since: " + time);
	}
}

var myDevice = new apn.Device("b924c35b16d3bfa719aef8ddae6846a6f0ab329da9b1cd90849bd33423b29ef9");
var note = new apn.Notification();

note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
note.badge = 3;
note.sound = "ping.aiff";
note.alert = "\uD83D\uDCE7 \u2709 You have a new message";
note.payload = {'messageFrom': 'Caroline'};

apnConnection.pushNotification(note, myDevice);

console.log("hye");
*/
/* noti end */


if(require.main === module) {
  app.start();
}
